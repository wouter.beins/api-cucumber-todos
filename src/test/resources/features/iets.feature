Feature: Test the todos api

  Scenario: Login with invalid credentials
    Given A user is on the login page
    When A valid user fills in the wrong password
    Then An error message appears